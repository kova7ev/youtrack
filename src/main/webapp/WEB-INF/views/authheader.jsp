<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<div class="authbar">
	<span>Dear <strong>${loggedinuser}</strong>, Welcome to
		YouTrack.
	</span> <span class="floatRight"><a href="<c:url value="/logout" />">Logout</a></span>

	<table>
		<tr>
			<sec:authorize access="hasRole('ADMIN') or hasRole('DBA')">
				<td><a href="/list"><span
						style="font-size: 12px; margin-right: 10px">Users</span></a></td>
			</sec:authorize>
			<sec:authorize access="hasRole('ADMIN') or hasRole('DBA')">
				<td><a href="/projects"><span
						style="font-size: 12px; margin-right: 10px">All projects</span></a></td>
			</sec:authorize>
			<td><a href="/myprojects"><span
					style="font-size: 12px; margin-right: 10px">My projects</span></a></td>
			<td><a href="/activities"><span
					style="font-size: 12px; margin-right: 10px">Activity</span></a></td>
		</tr>
	</table>
</div>